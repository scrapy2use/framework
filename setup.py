#!/usr/bin/env python
from os import path
from setuptools import setup, find_packages

here = path.abspath(path.dirname(__file__))
with open(path.join(here, 'requirements.txt'), encoding='utf-8') as f:
    all_reqs = f.read().split('\n')

install_requires = [x.strip() for x in all_reqs if 'git+' not in x]

setup(
    name='scrapy2use',
    version='0.3.0',
    license='MIT',
    description='Admin ui for spider service',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    url='https://github.com/scrapy2use/framework',
    author='TAYAA Med Amine',
    author_email='tayamino@gmail.com',
    include_package_data=True,
    packages=find_packages(exclude=['tests*']),
    install_requires=install_requires,
    entry_points={
        'console_scripts': {
            'scrapy2use = scrapy2use.run:main',
            'scrapy2web = scrapy2web.run:main',
        },
    },
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.5',
    ],
)
