from scrapy2use.items import *

#*******************************************************************************

class DistrosItem(scrapy.Item):
    title = scrapy.Field()
    data = scrapy.Field()

################################################################################

class Offensive_Server(scrapy.Item):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

class Offensive_Mobile(scrapy.Item):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

class Offensive_Device(scrapy.Item):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()
