from scrapy2use.items import *

#*******************************************************************************

class BaseItem(AbstractItem):
    class Parse:
        alias = 'orig'

################################################################################

class Author(BaseItem):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

#*******************************************************************************

class Publisher(BaseItem):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

class Oeuvre(BaseItem):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

################################################################################

class SacredText(BaseItem):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()
