# -*- coding: utf-8 -*-

from crawl.phybase.utils import *

####################################################################################

class SacredTextsSpider(BasicSpider):
    name = "sacred_texts"
    allowed_domains = ["www.sacred-texts.com"]
    start_urls = (
        'https://www.sacred-texts.com/index.htm',
    )

    rules = (
        Rule(LinkExtractor(allow=r"\/arabe\/Associations\.aspx"), callback='parse_assoc', follow=True),

        Rule(LinkExtractor(allow=r"\/arabe\/Legislations\/DernierBulletinOfficiel\.aspx"), callback='parse_bo_curr', follow=True),
        Rule(LinkExtractor(allow=r"\/arabe\/Legislations\/BulletinsOfficielsAns\.aspx"), callback='parse_bo_last', follow=True),
    )

    def parse(self,response):
        pass

    def parse_assoc(self,response):
        for item in response.xpath("//a[contains(@id,'_lstLinks_linkHyp_')]"):
            obj = SacredText()

            obj['orig'] = self.name
            obj['path'] = response.url
            obj['type'] = 'unknown'

            obj['when'] = str(datetime.now()) # item.xpath(".//tr[3]/td/text()").extract()
            obj['name'] = item.xpath("./text()").extract()
            obj['link'] = item.xpath("./@href").extract()

            yield obj

    def parse_bo_curr(self,response):
        prnt = response.xpath("//div[@class='dnnForm']//table")

        item = BulletinOfficiel()

        item['orig'] = self.name
        item['path'] = response.url
        item['type'] = 'bulletin'

        item['when'] = prnt.xpath(".//tr[3]/td/text()").extract()
        item['name'] = prnt.xpath(".//tr[4]//p/text()").extract()
        item['link'] = prnt.xpath(".//tr[2]//a/@href").extract()

        return item

    def parse_bo_last(self,response):
        prnt = response.xpath("//div[@class='dnnForm']//table")

        item = SacredText()

        item['orig'] = self.name
        item['path'] = response.url
        item['type'] = 'bulletin'

        item['when'] = prnt.xpath(".//tr[3]/td/text()").extract()
        item['name'] = prnt.xpath(".//tr[4]//p/text()").extract()
        item['link'] = prnt.xpath(".//tr[2]//a/@href").extract()

        return item
