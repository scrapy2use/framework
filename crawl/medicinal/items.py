from scrapy2use.items import *

#*******************************************************************************

class BaseItem(AbstractItem):
    pass

################################################################################

class Substance(BaseItem):
    link = scrapy.Field()
    name = scrapy.Field()

#*******************************************************************************

class Laboratory(BaseItem):
    link = scrapy.Field()
    name = scrapy.Field()

#*******************************************************************************

class Pharmaceutical(BaseItem):
    link = scrapy.Field()
    name = scrapy.Field()

################################################################################

class Pronostic(BaseItem):
    link = scrapy.Field()
    name = scrapy.Field()

#*******************************************************************************

class Medicament(BaseItem):
    link = scrapy.Field()
    name = scrapy.Field()

    info = scrapy.Field()
    code = scrapy.Field()
    mole = scrapy.Field()

    brand = scrapy.Field()
    famil = scrapy.Field()
    maker = scrapy.Field()

    type = scrapy.Field()
    pack = scrapy.Field()
    cons = scrapy.Field()
    ages = scrapy.Field()

    etat = scrapy.Field()
    cnss = scrapy.Field()
    sale = scrapy.Field()
    pcep = scrapy.Field()
    tier = scrapy.Field()
    gros = scrapy.Field()

    prix_c = scrapy.Field()
    prix_v = scrapy.Field()
    prix_h = scrapy.Field()
    prix_r = scrapy.Field()

    attr = scrapy.Field()

    class Parse:
        alias = 'link'
        field = [
            'name','link',
            'info','code','mole',
            #'brand','famil','maker',
            #'type','pack','cons','ages',
            #'etat','cnss','sale','pcep','tier','gros',
            #'prix_c','prix_v','prix_h','prix_r',
        ]
        realm = 'medicinal'
