from scrapy2use.settings import *

APP_ID = 'it_issal'

SPIDER_MODULES = [
    'scrapy2use.spiders',
    ##'crawl.spider.corporate',
    #'crawl.spider.gov_ma',
    ##'crawl.spider.research',
    'crawl.%s.spider' % APP_ID,
]
NEWSPIDER_MODULE = 'crawl.%s.spider' % APP_ID

# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    #'scrapy2use.pipelines.SQLdb.MySQL': 50,
    #'scrapy2use.pipelines.NoSQL.ParseServer': 50,
    'scrapy2use.pipelines.Graph.Neo4j': 50,
}

BOT_NAME = APP_ID
