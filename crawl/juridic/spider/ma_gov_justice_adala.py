# -*- coding: utf-8 -*-

"""
from py2neo import Graph as Neo4Graph, Node as Neo4Node, Relationship as Neo4Edge

Neo4j = Neo4Graph(
    host='hobby-kdoodghfkfpcgbkegpllfjml.dbs.graphenedb.com',
    http_port=24780,
    https_port=24780,
    #bolt_port=24780,
    user='jevad',
    password='b.ju3jtIrW6gCO.jcLnxA6Oi7cZ7eEv',
secure=True)
"""

from crawl.juridic.utils import *

class MaGovJusticeAdalaSpider(CrawlSpider):
    name = "ma_gov_justice_adala"
    allowed_domains = ["adala.justice.gov.ma"]

    start_urls = (
        'http://adala.justice.gov.ma/AR/home.aspx',
    )

    rules = (
        Rule(LinkExtractor(allow=('AR\/home\.aspx', )), follow=True, callback='parse_generic'),

        Rule(LinkExtractor(allow=('AR\/Legislation\/TextesJuridiques\.aspx', )), follow=True, callback='parse_generic'),
        Rule(LinkExtractor(allow=('AR\/Legislation\/textesjuridiques_(.*)\.aspx', )), callback='parse_legislation'),

        Rule(LinkExtractor(allow=('AR\/Conventions\/ConventionsPays\.aspx', )), follow=True, callback='parse_generic'),
        Rule(LinkExtractor(allow=('AR\/Conventions\/ConventionsInter\.aspx', )), callback='parse_convention'),
        Rule(LinkExtractor(allow=('AR\/Conventions\/ConventionsArabes\.aspx', )), callback='parse_convention'),
        Rule(LinkExtractor(allow=('AR\/Conventions\/Bilaterales\/(.*)\/index\.aspx', )), callback='parse_bilaterale'),

        Rule(LinkExtractor(allow=('AR\/RechercheClassifications\.aspx', )), follow=True, callback='parse_publication'),

        Rule(LinkExtractor(allow=('AR\/Jurisprudence\/Jurisprudence\.aspx', )), follow=True, callback='parse_generic'),
        Rule(LinkExtractor(allow=('AR\/Jurisprudence\/JP_(administratif|commerce|administratifpi)\.aspx', )), follow=True, callback='parse_generic'),
        Rule(LinkExtractor(allow=('AR\/Jurisprudence\/JPC_(casablanca|marrakech|fes)\.aspx', )), follow=True, callback='parse_generic'),
        Rule(LinkExtractor(allow=('AR\/Jurisprudence\/JP_famille\.aspx', )), callback='parse_juriprudence'),
        Rule(LinkExtractor(allow=('AR\/Jurisprudence\/JPC_CSfes\.aspx', )), callback='parse_juriprudence'),
        Rule(LinkExtractor(allow=('AR\/Jurisprudence\/JPC_(\w+)_(\d+)\.aspx', )), callback='parse_juriprudence'),
        Rule(LinkExtractor(allow=('AR\/Jurisprudence\/JP_administratif_(\w+)\.aspx', )), callback='parse_juriprudence'),
        Rule(LinkExtractor(allow=('AR\/Jurisprudence\/JP_administratifpi_(\w+)_(\d+)\.aspx', )), callback='parse_juriprudence'),

        Rule(LinkExtractor(allow=('AR\/Rapports\/Institutions\.aspx', )), follow=True, callback='parse_generic'),
        Rule(LinkExtractor(allow=('AR\/Rapports\/Rapports_(\w+)\.aspx', )), follow=True, callback='parse_generic'),
        Rule(LinkExtractor(allow=('AR\/Rapports\/Rapports_(\w+)_(\d+)\.aspx', )), callback='parse_rapport'),

        Rule(LinkExtractor(allow=('AR\/OutilsPratiques\/OutilsPratiques\.aspx', )), follow=True, callback='parse_outilpratic'),
        Rule(LinkExtractor(allow=('AR\/OutilsPratiques\/Procedures\/TP\/procedures_TPI\.aspx', )), follow=True, callback='parse_procedure'),

        #Rule(LinkExtractor(allow=('production\/OutilsPratiques\/ar\/Sommaire\.htm', )), follow=True, callback='parse_sommaire'),
        #Rule(LinkExtractor(allow=('production\/OutilsPratiques\/ar\/LexiqueFamilleAR\/index\.htm', )), follow=True, callback='parse_lexic_home'),
        #Rule(LinkExtractor(allow=('production\/OutilsPratiques\/ar\/LexiqueFamilleAR\/SRC\/(\w+)\.htm', )), follow=True, callback='parse_lexic_list'),
        #Rule(LinkExtractor(allow=('production\/OutilsPratiques\/ar\/LexiqueFamilleAR\/SRC\/(\w+)\-(\d+)\.htm', )), follow=True, callback='parse_lexic_item'),

        Rule(LinkExtractor(allow=('AR\/Statistiques\/Statistiques\.aspx', )), follow=True, callback='parse_generic'),
        Rule(LinkExtractor(allow=('AR\/Statistiques\/statistiquesactivite(\d+)\.aspx', )), callback='parse_statistic'),

        Rule(LinkExtractor(allow=('AR\/Etudes_Ouvrages\/Etudes_Ouvrages\.aspx', )), follow=True, callback='parse_generic'),
        Rule(LinkExtractor(allow=('AR\/Etudes_Ouvrages\/Ouvrages\.aspx', )), follow=True, callback='parse_ouvrage'),
        #Rule(LinkExtractor(allow=('AR\/Etudes_Ouvrages\/Etudes\.aspx', )), follow=True, callback='parse_generic'),
        #Rule(LinkExtractor(allow=('AR\/Etudes_Ouvrages\/Etudes_(\w+)\.aspx', )), follow=True, callback='parse_etudes'),

        #Rule(LinkExtractor(allow=('AR\/Guides_Manuels\/guides_manuels\.aspx', )), follow=True, callback='parse_manuels'),
        #Rule(LinkExtractor(allow=('AR\/Revues_juridiques\/Revues_juridiques\.aspx', )), follow=True, callback='parse_revues'),

        #Rule(LinkExtractor(allow=('category\.php', ), deny=('subsection\.php', ))),
    )

    ############################################################################

    def cleanup(self, target, selector):
        raw = target.xpath(selector).extract()

        if len(raw):
            return raw[0].strip()
        else:
            return ""

    def parse_generic(self,response):
        yield AdalaPage(
            fqdn=self.allowed_domains[0],
            prnt=response.meta.get('parent',self.start_urls[0]),

            name=self.cleanup(response, "//div[@class='titre_cadre']/text()"),
            link=response.url,
        )

        for item in response.xpath("//table[@id='page']//table//table//table//table//tr"):
            entry = AdalaPage(
                fqdn=self.allowed_domains[0],
                prnt=response.url,

                name=self.cleanup(item, './td[1]/span/text()'),
                link=self.cleanup(item, './td[2]/a/@href'),
            )

            entry['link'] = urljoin(response.url, entry['link'])

            yield entry

    ############################################################################

    def parse_legislation(self,response):
        yield AdalaPage(
            fqdn=self.allowed_domains[0],
            prnt=response.meta.get('parent',self.start_urls[0]),

            name=self.cleanup(response, "//div[@class='titre_cadre']/text()"),
            link=response.url,
        )

        for item in response.xpath("//table[@id='page']//table//table//table//table//tr"):
            entry = AdalaLegislation(
                fqdn=self.allowed_domains[0],
                prnt=response.url,

                name=self.cleanup(item, './td[1]/span/text()'),
                link=self.cleanup(item, './td[2]/a/@href'),

                docx=self.cleanup(item, './td[3]/a/@href'),
                p_ar=self.cleanup(item, './td[2]/a/@href'),
                p_fr=self.cleanup(item, './td[4]/a/@href'),
            )

            entry['link'] = urljoin(response.url, entry['link'])

            entry['docx'] = urljoin(response.url, entry['docx']).replace('../','')
            entry['p_ar'] = urljoin(response.url, entry['p_ar'])
            entry['p_fr'] = urljoin(response.url, entry['p_fr'])

            yield entry

    def parse_convention(self,response):
        yield AdalaPage(
            fqdn=self.allowed_domains[0],
            prnt=response.meta.get('parent',self.start_urls[0]),

            name=self.cleanup(response, "//div[@class='titre_cadre']/text()"),
            link=response.url,
        )

        for item in response.xpath("//table[@id='page']//table//table//table//table//tr"):
            entry = AdalaConvention(
                fqdn=self.allowed_domains[0],
                prnt=response.url,

                name=self.cleanup(item, './td[1]/span/text()'),
                link=self.cleanup(item, './td[2]/a/@href'),

                #docx=self.cleanup(item, './td[3]/a/@href'),
                #p_ar=self.cleanup(item, './td[2]/a/@href'),
                #p_fr=self.cleanup(item, './td[4]/a/@href'),
            )

            entry['link'] = urljoin(response.url, entry['link'])

            #entry['docx'] = urljoin(response.url, entry['docx']).replace('../','')
            #entry['p_ar'] = urljoin(response.url, entry['p_ar'])
            #entry['p_fr'] = urljoin(response.url, entry['p_fr'])

            yield entry

    def parse_bilaterale(self,response):
        yield AdalaPage(
            fqdn=self.allowed_domains[0],
            prnt=response.meta.get('parent',self.start_urls[0]),

            name=self.cleanup(response, "//div[@class='titre_cadre']/text()"),
            link=response.url,
        )

        for item in response.xpath("//table[@id='page']//table//table//table//table//tr"):
            alias = self.cleanup(item, './td[1]/span/text()')

            entry = AdalaBilaterale(
                fqdn=self.allowed_domains[0],
                prnt=response.url,

                name=self.cleanup(item, './td[1]/span/text()'),
                link=self.cleanup(item, './td[2]/a/@href'),

                #docx=self.cleanup(item, './td[3]/a/@href'),
                #p_ar=self.cleanup(item, './td[2]/a/@href'),
                #p_fr=self.cleanup(item, './td[4]/a/@href'),
            )

            entry['link'] = urljoin(response.url, entry['link'])

            #entry['docx'] = urljoin(response.url, entry['docx']).replace('../','')
            #entry['p_ar'] = urljoin(response.url, entry['p_ar'])
            #entry['p_fr'] = urljoin(response.url, entry['p_fr'])

            yield entry

    def parse_publication(self,response):
        yield AdalaPage(
            fqdn=self.allowed_domains[0],
            prnt=response.meta.get('parent',self.start_urls[0]),

            name=self.cleanup(response, "//div[@class='titre_cadre']/text()"),
            link=response.url,
        )

        for item in response.xpath("//table[@id='ctrlResult_dgResult']//tr"):
            entry = AdalaPublication(
                order=self.cleanup(item, './td[11]/a/text()'),
                rpath=self.cleanup(item, './td[11]/a/@href'),

                trust=self.cleanup(item, './td[10]/span/text()'),
                index=self.cleanup(item, './td[9]/text()'),

                dateh=self.cleanup(item, './td[8]/text()'),
                datem=self.cleanup(item, './td[7]/text()'),

                label=self.cleanup(item, './td[6]/a/text()'),
                rlink=self.cleanup(item, './td[6]/a/@href'),

                owner=self.cleanup(item, './td[5]/span/text()'),
                categ=self.cleanup(item, './td[2]/a/@href'),
            )

            entry['rpath'] = urljoin(response.url, entry['rpath'])
            entry['rlink'] = urljoin(response.url, entry['rlink'])
            entry['categ'] = entry['categ'].replace("http://adala.justice.gov.ma",'')
            entry['categ'] = entry['categ'].replace("/AR/RechercheClassifications.aspx",'')
            entry['categ'] = entry['categ'].replace("?pathClassifications=Root/",'')
            entry['parnt'] = response.url

            yield entry

    def parse_juriprudence(self,response):
        yield AdalaPage(
            fqdn=self.allowed_domains[0],
            prnt=response.meta.get('parent',self.start_urls[0]),

            name=self.cleanup(response, "//div[@class='titre_cadre']/text()"),
            link=response.url,
        )

        for item in response.xpath("//table[@id='page']//table//table//table//table//tr"):
            alias = self.cleanup(item, './td[1]/span/text()')

            entry = AdalaJuriPrudence(
                fqdn=self.allowed_domains[0],
                prnt=response.url,

                name=self.cleanup(item, './td[1]/span/text()'),
                link=self.cleanup(item, './td[2]/a/@href'),

                #docx=self.cleanup(item, './td[3]/a/@href'),
                #p_ar=self.cleanup(item, './td[2]/a/@href'),
                #p_fr=self.cleanup(item, './td[4]/a/@href'),
            )

            entry['link'] = urljoin(response.url, entry['link'])

            #entry['docx'] = urljoin(response.url, entry['docx']).replace('../','')
            #entry['p_ar'] = urljoin(response.url, entry['p_ar'])
            #entry['p_fr'] = urljoin(response.url, entry['p_fr'])

            yield entry

    def parse_rapport(self,response):
        yield AdalaPage(
            fqdn=self.allowed_domains[0],
            prnt=response.meta.get('parent',self.start_urls[0]),

            name=self.cleanup(response, "//div[@class='titre_cadre']/text()"),
            link=response.url,
        )

        for item in response.xpath("//table[@id='page']//table//table//table//table//tr"):
            alias = self.cleanup(item, './td[1]/span/text()')

            entry = AdalaRapport(
                fqdn=self.allowed_domains[0],
                prnt=response.url,

                name=self.cleanup(item, './td[1]/span/text()'),
                link=self.cleanup(item, './td[2]/a/@href'),

                #docx=self.cleanup(item, './td[3]/a/@href'),
                #p_ar=self.cleanup(item, './td[2]/a/@href'),
                #p_fr=self.cleanup(item, './td[4]/a/@href'),
            )

            entry['link'] = urljoin(response.url, entry['link'])

            #entry['docx'] = urljoin(response.url, entry['docx']).replace('../','')
            #entry['p_ar'] = urljoin(response.url, entry['p_ar'])
            #entry['p_fr'] = urljoin(response.url, entry['p_fr'])

            yield entry

    ############################################################################

    def extract_page(self,response):
        sky = Neo4Node("LawPage", link=response.meta.get('parent',self.start_urls[0]))

        #Neo4j.merge(sky, "LawPage", "link")

        prn = Neo4Node("LawPage", link=response.url)

        #Neo4j.merge(prn, "LawPage", "link")

        #Neo4j.merge(Neo4Edge(sky,"REFERS",prn))

        url = None

        for item in response.xpath("//table[@id='page']//table//table//table//table//tr"):
            key = item.xpath('./td[1]//span/text()').extract()[0].strip()

            try:
                url = item.xpath('./td[1]//a/@href').extract()[0].strip()
            except Exception:
                raise ex

                url = None

            if url is not None:
                req,elm,vec = None,None,None

                dest = urljoin(response.url, url)

                if response.url.endswith('TextesJuridiques.aspx'):
                    req = scrapy.Request(dest, callback=self.parse_law_text)
                elif response.url.endswith('Conventions.aspx'):
                    req = scrapy.Request(dest, callback=self.parse_law_conv)

                #if 'textesjuridiques_' in response.url:
                #    elm = Neo4Node("LawText", name=key, link=dest)
                #    vec = "DEFINE"
                #
                #    #Neo4j.merge(elm, "LawText", "link")
                #elif 'Conventions' in response.url:
                #    elm = Neo4Node("LawConv", name=key, link=dest)
                #    vec = "DEFINE"
                #
                #    #Neo4j.merge(elm, "LawConv", "link")
                #else:
                #    elm = Neo4Node("LawPage", name=key, link=dest)
                #    vec = "REFERS"
                #
                #    #Neo4j.merge(elm, "LawPage", "link")

                if req is None:
                    req = scrapy.Request(dest)

                yield req
