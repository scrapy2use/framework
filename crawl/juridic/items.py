from scrapy2use.items import *

#*******************************************************************************

class AdalaBase(scrapy.Item):
    fqdn = scrapy.Field()
    prnt = scrapy.Field()

    name = scrapy.Field()
    link = scrapy.Field()

#*******************************************************************************

class AdalaPage(AdalaBase):
    class Neo4j:
        label = "AdalaPage"
        relat = [
            ("REFERS", 'prnt', "AdalaPage", 'link'),
        ]
        alias = 'link'

#*******************************************************************************

class AdalaPublication(AdalaBase):
    class Neo4j:
        label = "Publication"
        relat = [
            ("REFERS", 'parnt', "AdalaPage", 'link'),
        ]
        alias = 'alias'

    order = scrapy.Field()
    trust = scrapy.Field()
    index = scrapy.Field()

    alias = scrapy.Field()
    label = scrapy.Field()

    owner = scrapy.Field()
    categ = scrapy.Field()

    rlink = scrapy.Field()
    rpath = scrapy.Field()

    dateh = scrapy.Field()
    datem = scrapy.Field()

################################################################################

class SggItem(scrapy.Item):
    title = scrapy.Field()
    data = scrapy.Field()

class SgggovmaLaw(scrapy.Item):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

################################################################################

class AdalaLegislation(AdalaBase):
    docx = scrapy.Field()
    p_ar = scrapy.Field()
    p_fr = scrapy.Field()

    class Neo4j:
        label = "Legislation"
        relat = [
            ("REFERS", 'prnt', "AdalaPage", 'link'),
        ]
        alias = 'docx'

    class Parse:
        realm = 'juridic'
        model = "Legislation"
        alias = 'docx'

class AdalaConvention(AdalaBase):
    class Neo4j:
        label = "Convention"
        relat = [
            ("REFERS", 'prnt', "AdalaPage", 'link'),
        ]
        alias = 'html'

    class Parse:
        realm = 'juridic'
        model = "Convention"
        alias = 'html'

class AdalaBilaterale(AdalaBase):
    class Neo4j:
        label = "Bilaterale"
        relat = [
            ("REFERS", 'prnt', "AdalaPage", 'link'),
        ]
        alias = 'link'

    class Parse:
        realm = 'juridic'
        model = "Bilaterale"
        alias = 'link'

class AdalaJuriPrudence(AdalaBase):
    class Neo4j:
        label = "JuriPrudence"
        relat = [
            ("REFERS", 'prnt', "AdalaPage", 'link'),
        ]
        alias = 'link'

    class Parse:
        realm = 'juridic'
        model = "JuriPrudence"
        alias = 'link'

class AdalaRapport(AdalaBase):
    class Neo4j:
        label = "Rapport"
        relat = [
            ("REFERS", 'prnt', "AdalaPage", 'link'),
        ]
        alias = 'link'

    class Parse:
        realm = 'juridic'
        model = "Rapport"
        alias = 'link'
