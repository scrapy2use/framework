from scrapy2use.items import *

#*******************************************************************************

class BaseItem(AbstractItem):
    class Parse:
        alias = 'rlink'

#*******************************************************************************

class Cuisine(BaseItem):
    wsite = scrapy.Field()
    rlink = scrapy.Field()

    alias = scrapy.Field()
    title = scrapy.Field()

    cooks = scrapy.Field()

#*******************************************************************************

class Occasion(BaseItem):
    wsite = scrapy.Field()
    rlink = scrapy.Field()

    alias = scrapy.Field()
    title = scrapy.Field()

    cooks = scrapy.Field()

#*******************************************************************************

class Lesson(BaseItem):
    wsite = scrapy.Field()
    rlink = scrapy.Field()

    alias = scrapy.Field()
    title = scrapy.Field()

################################################################################

class Recipe(BaseItem):
    wsite = scrapy.Field()
    rlink = scrapy.Field()

    alias = scrapy.Field()
    title = scrapy.Field()

    descr = scrapy.Field()
    logos = scrapy.Field()

    items = scrapy.Field()
    steps = scrapy.Field()
    foods = scrapy.Field()

    categ = scrapy.Field()
    group = scrapy.Field()
    words = scrapy.Field()

    class Parse:
        field = (
            'rlink','alias','title','descr','logos',
            #'wsite','rlink','alias','title','descr','logos',
            #'items','steps','foods','categ','group','words',
            'foods','categ','group','words',
        )
        alias = 'rlink'

#*******************************************************************************

class Ingredient(BaseItem):
    cook = scrapy.Field()
    name = scrapy.Field()

    unit = scrapy.Field()
    size = scrapy.Field()

#*******************************************************************************

class Aliment(BaseItem):
    wsite = scrapy.Field()
    rlink = scrapy.Field()

    alias = scrapy.Field()
    title = scrapy.Field()

    descr = scrapy.Field()
    logos = scrapy.Field()

################################################################################

class MantoojRecord(BaseItem):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

    class Parse:
        alias = 'path'

################################################################################
