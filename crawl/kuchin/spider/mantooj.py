# -*- coding: utf-8 -*-

from crawl.kuchin.utils import *

class MantoojSpider(BasicSpider):
    name = "mantooj"
    allowed_domains = ["www.mantooj.net"]

    start_urls = (
        'http://www.mantooj.net/index.php?option=com_gs1&controller=search&rsid=2&Itemid=116&lang=fr',
        'http://www.mantooj.net/index.php?option=com_gs1&controller=search&rsid=1&Itemid=117&lang=fr',
        'http://www.mantooj.net/index.php?option=com_content&view=article&id=4&Itemid=137&lang=fr',
    )

    def parse(self,response):
        handler = self.parse_static

        qs = dict(parse_qsl(urlparse(response.url).query))

        if 'controller' in qs:
            if qs['controller']=='product':
                handler = self.parse_category

                #option=com_gs1&controller=product&layout=detail&id=842&lang=fr
            elif qs['controller']=='search':
                handler = self.parse_category

                #option=com_gs1&controller=search&layout=productlist&type=p&lang=fr&classP=67010100
        elif 'view' in qs:
            if qs['view']=='article':
                handler = self.parse_produit

                #option=com_content&view=article&id=4&Itemid=137&lang=fr

        if callable(handler):
            handler(response)

    def parse_static(self,response):
        if False:
            yield None

    def parse_category(self,response):
        for item in response.xpath("//a[contains(@id,'_lstLinks_linkHyp_')]"):
            obj = SgggovmaLaw()

            obj['orig'] = self.name
            obj['path'] = response.url
            obj['type'] = 'unknown'

            obj['when'] = str(datetime.now()) # item.xpath(".//tr[3]/td/text()").extract()
            obj['name'] = item.xpath("./text()").extract()
            obj['link'] = item.xpath("./@href").extract()

            yield obj

    def parse_produit(self,response):
        prnt = response.xpath("//div[@class='dnnForm']//table")

        item = BulletinOfficiel()

        item['orig'] = self.name
        item['path'] = response.url
        item['type'] = 'bulletin'

        item['when'] = prnt.xpath(".//tr[3]/td/text()").extract()
        item['name'] = prnt.xpath(".//tr[4]//p/text()").extract()
        item['link'] = prnt.xpath(".//tr[2]//a/@href").extract()

        return item
