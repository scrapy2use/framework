# -*- coding: utf-8 -*-

from crawl.kuchin.utils import *

class CuisineDuMarocSpider(CrawlSpider):
    name = "cuisine_du_maroc"
    allowed_domains = ["cuisinedumaroc.com"]

    start_urls = (
        #'http://cuisinedumaroc.com/',
        'http://www.cuisinedumaroc.com/recette/chebakia-ramadan/',
    )

    rules = (
        Rule(LinkExtractor(allow=('cuisine\/(.*)\/', )), follow=True),
        Rule(LinkExtractor(allow=('occasion\/(.*)\/', )), follow=True),
        Rule(LinkExtractor(allow=('theme\/(.*)\/', )), follow=True),

        Rule(LinkExtractor(allow=('recettes\/(.*)\/', )), follow=True),
        Rule(LinkExtractor(allow=('lesrecettes\/(.*)\/', )), follow=True),

        Rule(LinkExtractor(allow=('recette\/(.*)\/', )), callback='parse_recipe'),
        Rule(LinkExtractor(allow=('recette\/(.*)\/', )), callback='parse_recipe'),
        #Rule(LinkExtractor(allow=('recipe\/(.*)\/', )), callback='parse_video'),
    )

    def cleanup(self, target, selector, flat=False):
        raw = target.xpath(selector).extract()

        if len(raw):
            obj = raw[0].strip()

            if flat:
                return self.flat(obj)
        else:
            return ""

    def cleaner(self, target, selector):
        lst = target.xpath(selector).extract()

        return [self.flat(obj) for obj in lst]

    def flat(self, value):
        resp = value.replace("http://www.cuisinedumaroc.com",'')

        if resp.endswith('/'):
            resp = resp[:-1]

        resp = resp.replace("/cuisine/",'').replace("/ingredient/",'')
        resp = resp.replace("/occasion/",'').replace("/theme/",'')
        resp = resp.replace("/recette/",'').replace("/recettes/",'')

        return resp

    def parse_recipe(self,response):
        entry = Recipe(
            alias=response.url.replace("recette/",""),
            title=self.cleanup(response, "//h1[@class='title fn']/text()"),

            categ=self.cleanup(response, "//span[@class='type']/a/@href", flat=True),
            logos=self.cleanup(response, "//img[@class='photo']//@src"),
            descr=self.cleanup(response, "//div[@class='info-left instructions']//p[1]/text()"),
            steps="\n".join(response.xpath("//div[@class='info-left instructions']//p[1<position()]/text()").extract()),

            group=response.xpath("//ul[@class='recipe-cat-info clearfix']//a/@href").extract(),
            items=response.xpath("//li[@class='ingredient']/text()").extract(),
            words=response.xpath("//span[@class='tags']/a/@href").extract(),
            foods=response.xpath("//span[@class='ingredient']/a/@href").extract(),

            rlink=response.url,
            #webns=self.allowed_domains[0],
            #ances=response.meta.get('parent',self.start_urls[0]),
        )

        for key in ('group','items','words','foods'):
            entry[key] = [self.flat(obj) for obj in entry[key]]

        yield entry

    def parse_video(self,response):
        entry = Recipe(
            alias=response.url.replace("recette/",""),
            title=self.cleanup(response, "//h1[@class='title fn']/text()"),

            categ=response.xpath("//ul[@class='recipe-cat-info clearfix']//a/@href").extract(),
            logos=self.cleanup(response, "//img[@class='photo']//@src"),
            infos="\n".join(response.xpath("//div[@class='info-left instructions']//p/text()").extract()),

            ingre=response.xpath("//li[@class='ingredient']/text()").extract(),

            rlink=response.url,
            webns=self.allowed_domains[0],
            ances=response.meta.get('parent',self.start_urls[0]),
        )

        yield entry
