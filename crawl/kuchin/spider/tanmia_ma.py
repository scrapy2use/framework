# -*- coding: utf-8 -*-

from crawl.kuchin.utils import *

####################################################################################

class TanmiaMaSpider(BasicSpider):
    name = "tanmia.ma"
    allowed_domains = ["asso.tanmia.ma"]
    start_urls = (
        'http://asso.tanmia.ma/association/activities/all',
    )

    BASE_LINK = "http://asso.tanmia.ma"

    def parse(self, response):
        if ("/association/show/" in response.url):
            for item in self.parse_asso(response):
                yield item
        else:
            for link in response.xpath('//a[@class="act_search"]/@href').extract():
                u = self.BASE_LINK + link # response.urljoin(link)

                r = scrapy.Request(u, callback=self.parse_page)

                #r.meta['name']=distro

                yield r

    def parse_page(self,response):
        # search result
        for link in response.xpath('//div[@class="search-result"]//strong[@class="title"]//a[@class="link_color"]/@href').extract():
            u = self.BASE_LINK + link # response.urljoin(link)

            r = scrapy.Request(u, callback=self.parse_asso)

            #r.meta['name']=distro

            yield r

        # pagination
        for link in response.xpath('//div[@class="pagination"]//ul/li/a/@href').extract():
            u = self.BASE_LINK + link # response.urljoin(link)

            r = scrapy.Request(u, callback=self.parse_page)

            #r.meta['name']=distro

            yield r

    def parse_asso(self,response):
        item = Association()

        item['alias'] = response.url.replace('http://asso.tanmia.ma/association/show/','')
        item['title'] = response.xpath('//div[@class="container"]//h2/text()').extract()[0]

        item['phone'] = response.xpath('//div[@class="container"]//div[@class="clearfix"]/div[2]/div[1]/text()').extract()
        item['faxes'] = response.xpath('//div[@class="container"]//div[@class="clearfix"]/div[2]/div[2]/text()').extract()
        item['email'] = response.xpath('//div[@class="container"]//div[@class="clearfix"]/div[2]/div[3]/text()').extract()

        item['sites'] = response.xpath('//div[@class="container"]//div[@class="clearfix"]/div[2]/div[4]/text()').extract()
        item['pages'] = response.xpath('//div[@class="container"]//div[@class="clearfix"]/div[2]/div[5]/text()').extract()

        item['location'] = response.xpath('//div[@class="container"]//div[@class="clearfix"]/div[2]/div[6]/text()').extract()
        item['creation'] = response.xpath('//div[@class="container"]//div[@class="clearfix"]/div[2]/div[7]/text()').extract()

        item['mission'] = response.xpath('//div[@class="asso-head clearfix"]//div[@class="row-fluid"][1]//div[@class="span9"]/text()').extract()
        item['objects'] = response.xpath('//div[@class="asso-head clearfix"]//div[@class="row-fluid"][2]//div[@class="span9"]/text()').extract()

        item['populat'] = response.xpath('//div[@class="asso-head clearfix"]//div[@class="row-fluid"][3]//div[@class="span9"]/text()').extract()
        item['activ_p'] = response.xpath('//div[@class="asso-head clearfix"]//div[@class="row-fluid"][4]//div[@class="span9"]/text()').extract()
        item['activ_s'] = response.xpath('//div[@class="asso-head clearfix"]//div[@class="row-fluid"][5]//div[@class="span9"]/text()').extract()

        #item['phone'] = item['phone'].replace('\n',' ').strip().split('/')

        if len(item['populat']):
            item['populat'] = item['populat'][0].replace('\n',' ').strip().split(',')

        yield item
