# -*- coding: utf-8 -*-

# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy2use.utils import *

class JSON(object):
    def __init__(self):
        pass

    def process_item(self, item, spider):
        flag = False

        if hasattr(item,'JSON'):
            if not hasattr(item.JSON,'alias'):
                setattr(item.JSON, 'alias', item.__name__.lower())

            path = os.path.join(settings.JSON_FOLDER, spider.name)

            if not os.path.exists(path):
                os.system("mkdir -p " % path)

            path = os.path.join(settings.JSON_FOLDER, spider.name, "%s.json" % item.JSON.alias)

            if hasattr(item.JSON,'exclude'):
                if item.JSON.alias in item.JSON.exclude:
                    flag = False

            data = json.loads(open(path).read())

            with open(path,'a') as f:
                f.write(data + [item.__dict__])

        return item
