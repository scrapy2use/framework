# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy2use.utils import *

from py2neo import Graph as Neo4Graph, Node as Neo4Node, Relationship as Neo4Edge

class Neo4j(object):
    def __init__(self):
        self.driver = Neo4Graph(
            host='hobby-jhfjkjgcconogbkegjohcobl.dbs.graphenedb.com',
            port=24786,
            scheme='bolt',
            #https_port=24780,
            #bolt_port=24786,
            user='admin',
            password='b.qkzwiAmT48i4.ajFjXxhXV9rbC5gG',
        secure=True)

    def process_item(self, item, spider):
        if hasattr(item,'Neo4j'):
            obj = Neo4Node(item.Neo4j.label, **item.__dict__['_values'])

            self.driver.merge(obj, item.Neo4j.label, item.Neo4j.alias)

            if hasattr(item.Neo4j,'relat'):
                for rel,src,lbl,key in item.Neo4j.relat:
                    prn = Neo4Node(lbl, **{ key: item.__dict__['_values'][src] })

                    self.driver.merge(prn, lbl, key)

                    self.driver.merge(Neo4Edge(prn, rel, obj))

            #self.driver.merge(obj, item.Neo4j.label, item.Neo4j.alias)

        return item
