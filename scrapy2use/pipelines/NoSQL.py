# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy2use.utils import *

from parse_rest.datatypes import Function, Object, GeoPoint
from parse_rest.query import QueryResourceDoesNotExist
from parse_rest.connection import ParseBatcher
from parse_rest.core import ResourceRequestBadRequest, ParseError

from parse_rest.datatypes import Object
from parse_rest.user import User

import requests

class ParseServer(object):
    def __init__(self):
        self.rest = settings.PARSE_REST_KEY

    def linker(self, name, *args):
        return 'https://'+name+'.herokuapp.com/model/api/%s' % '/'.join(args)

    def fetch(self, model, uuid):
        req = requests.get(self.linker(model.realm, 'classes', model.table, uuid), headers={
            "X-Parse-Application-Id": model.realm,
            "X-Parse-REST-API-Key": self.rest,
            "Content-Type": "application/json",
        })

        res = req.json()

        return res

    def retrieve(self, model, uuid):
        req = requests.get(self.linker(model.realm, 'classes', model.table), params={
            'where':'{"'+model.alias+'":"'+uuid+'"}',
        }, headers={
            "X-Parse-Application-Id": model.realm,
            "X-Parse-REST-API-Key": self.rest,
            "Content-Type": "application/json",
        })

        res = req.json()

        if len(res['results']):
            return res['results'][0]
        else:
            return None

    def create(self, model, item):
        uid = item[model.alias]

        req = requests.post(self.linker(model.realm, 'classes', model.table), data=json.dumps(item), headers={
            "X-Parse-Application-Id": model.realm,
            "X-Parse-REST-API-Key": self.rest,
            "Content-Type": "application/json",
        })

        res = req.json()

        return res

    def process_item(self, item, spider):
        if hasattr(item,'Parse'):
            for alias,value in dict(
                realm=None,
                table=item.__class__.__name__.capitalize(),
                field=[],
            ).items():
                if not hasattr(item.Parse,alias):
                    setattr(item.Parse,alias,value)

            if item.Parse.realm not in settings.PARSE_WORLDS:
                item.Parse.realm = settings.PARSE_DEFAULT

            if not len(item.Parse.field):
                item.Parse.field = item.__dict__['_values'].keys()

            dat = dict([
                (x,item.__dict__['_values'][x])
                for x in item.Parse.field
                if x in item.__dict__['_values']
            ])

            nrw = dat[item.Parse.alias]

            obj = self.retrieve(item.Parse, nrw)

            if obj is None:
                obj = self.create(item.Parse, dat)
            else:
                print(obj)

        return item
