# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy2use.utils import *

try:
   import MySQLdb
except:
   pass

class MySQL(object):
    def __init__(self):
        self.conn = MySQLdb.connect(
            'localhost', 'root', '',
        'azalinc_api', charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()

    def backup_item(self, table, item, spider):
        f,v = [],[]

        for key in item.keys():
            if key not in ('attr',''):
                f.append("`%s`" % key)
                v.append("'%s'" % item[key])

        f = ', '.join(f)
        v = ', '.join(v)

        r = "INSERT INTO %s (%s) VALUES (%s)" % (table, f,v)

        try:
            self.cursor.execute(r)
            self.conn.commit()
        except (MySQLdb.Error,e):
            print("MySQL Error : %s" % r)

            print("Error %d: %s" % (e.args[0], e.args[1]))

    def process_item(self, item, spider):
        if type(item) is HealthDrug:
            self.backup_item('health_drugs', item, spider)

        return item
