# -*- coding: utf-8 -*-

import scrapy

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from datetime import datetime

class SgggovmaLaw(scrapy.Item):
    orig = scrapy.Field()
    path = scrapy.Field()
    type = scrapy.Field()

    when = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()

class SgggovmaSpider(scrapy.Spider):
    name = "sgggovma"
    allowed_domains = ["www.sgg.gov.ma"]
    start_urls = (
        'http://www.sgg.gov.ma/arabe/Legislations.aspx',
        'http://www.sgg.gov.ma/arabe/Associations.aspx',
    )

    rules = (
        Rule(LinkExtractor(allow=r"\/arabe\/Associations\.aspx"), callback='parse_assoc', follow=True),

        Rule(LinkExtractor(allow=r"\/arabe\/Legislations\/DernierBulletinOfficiel\.aspx"), callback='parse_bo_curr', follow=True),
        Rule(LinkExtractor(allow=r"\/arabe\/Legislations\/BulletinsOfficielsAns\.aspx"), callback='parse_bo_last', follow=True),
    )

    def parse(self,response):
        pass

    def parse_assoc(self,response):
        for item in response.xpath("//a[contains(@id,'_lstLinks_linkHyp_')]"):
            obj = SgggovmaLaw()

            obj['orig'] = self.name
            obj['path'] = response.url
            obj['type'] = 'unknown'

            obj['when'] = str(datetime.now()) # item.xpath(".//tr[3]/td/text()").extract()
            obj['name'] = item.xpath("./text()").extract()
            obj['link'] = item.xpath("./@href").extract()

            yield obj

    def parse_bo_curr(self,response):
        prnt = response.xpath("//div[@class='dnnForm']//table")

        item = BulletinOfficiel()

        item['orig'] = self.name
        item['path'] = response.url
        item['type'] = 'bulletin'

        item['when'] = prnt.xpath(".//tr[3]/td/text()").extract()
        item['name'] = prnt.xpath(".//tr[4]//p/text()").extract()
        item['link'] = prnt.xpath(".//tr[2]//a/@href").extract()

        return item

    def parse_bo_last(self,response):
        prnt = response.xpath("//div[@class='dnnForm']//table")

        item = SgggovmaLaw()

        item['orig'] = self.name
        item['path'] = response.url
        item['type'] = 'bulletin'

        item['when'] = prnt.xpath(".//tr[3]/td/text()").extract()
        item['name'] = prnt.xpath(".//tr[4]//p/text()").extract()
        item['link'] = prnt.xpath(".//tr[2]//a/@href").extract()

        return item
