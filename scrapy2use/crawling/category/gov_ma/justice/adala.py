# -*- coding: utf-8 -*-

from py2neo import Graph as Neo4Graph, Node as Neo4Node, Relationship as Neo4Edge
import scrapy
from urlparse import *

class AdalaItem(scrapy.Item):
    fqdn = scrapy.Field()
    link = scrapy.Field()
    prnt = scrapy.Field()

    name = scrapy.Field()
    unit = scrapy.Field()

    docx = scrapy.Field()
    p_ar = scrapy.Field()
    p_fr = scrapy.Field()

Neo4j = Neo4Graph(
    host='hobby-kdoodghfkfpcgbkegpllfjml.dbs.graphenedb.com',
    http_port=24780,
    https_port=24780,
    #bolt_port=24780,
    user='jevad',
    password='b.ju3jtIrW6gCO.jcLnxA6Oi7cZ7eEv',
secure=True)

class AdalaSpider(scrapy.Spider):
    name = "adala"
    allowed_domains = ["adala.justice.gov.ma"]
    start_urls = (
        'http://adala.justice.gov.ma/AR/Legislation/TextesJuridiques.aspx',
        'http://adala.justice.gov.ma/AR/Conventions/Conventions.aspx',
        #'http://adala.justice.gov.ma/AR/Jurisprudence/Jurisprudence.aspx',
        #'http://adala.justice.gov.ma/AR/Rapports/Institutions.aspx',
    )

    def parse(self, response):
        sky = Neo4Node("LawPage", link=response.meta.get('parent',self.start_urls[0]))

        Neo4j.merge(sky, "LawPage", "link")

        prn = Neo4Node("LawPage", link=response.url)

        Neo4j.merge(prn, "LawPage", "link")

        Neo4j.merge(Neo4Edge(sky,"REFERS",prn))

        url = None

        for item in response.xpath("//table[@id='page']//table//table//table//table//tr"):
            key = item.xpath('./td[1]//span/text()').extract()[0].strip()

            try:
                url = item.xpath('./td[1]//a/@href').extract()[0].strip()
            except Exception,ex:
                raise ex

                url = None

            if url is not None:
                req,elm,vec = None,None,None

                dest = urljoin(response.url, url)

                if response.url.endswith('TextesJuridiques.aspx'):
                    req = scrapy.Request(dest, callback=self.parse_law_text)
                #elif response.url.endswith('Conventions.aspx'):
                #    req = scrapy.Request(dest, callback=self.parse_law_conv)
                else:
                    req = scrapy.Request(dest, callback=self.parse)

                if 'textesjuridiques_' in response.url:
                    elm = Neo4Node("LawText", name=key, link=dest)
                    vec = "DEFINE"
 
                    Neo4j.merge(elm, "LawText", "link")
                elif 'Conventions' in response.url:
                    elm = Neo4Node("LawConv", name=key, link=dest)
                    vec = "DEFINE"
 
                    Neo4j.merge(elm, "LawConv", "link")
                else:
                    elm = Neo4Node("LawPage", name=key, link=dest)
                    vec = "REFERS"
 
                    Neo4j.merge(elm, "LawPage", "link")

                rel = Neo4Edge(prn,vec,elm)

                Neo4j.merge(rel)

                if req is not None:
                    req.meta['parent'] = response.url

                    yield req

    def parse_law_text(self,response):
        for item in response.xpath("//table[@id='page']//table//table//table//table//tr"):
            alias = item.xpath('./td[1]/span/text()').extract()[0].strip()

            entry = AdalaItem(
                fqdn=self.allowed_domains[0],
                link=response.url,
                prnt=response.meta.get('parent',self.start_urls[0]),

                name=alias,
                unit=response.xpath("//div[@class='titre_cadre']/text()").extract()[0].strip(),

                docx=item.xpath('./td[3]/a/@href').extract()[0],
                p_ar=item.xpath('./td[2]/a/@href').extract()[0],
                p_fr=item.xpath('./td[4]/a/@href').extract()[0],
            )

            entry['docx'] = urljoin(response.url, entry['docx']).replace('../','')
            entry['p_ar'] = urljoin(response.url, entry['p_ar'])
            entry['p_fr'] = urljoin(response.url, entry['p_fr'])

            yield entry

            prn = Neo4Node("LawPage", link=entry['prnt'])

            Neo4j.merge(prn, "LawPage", "link")

            obj = Neo4Node("LawText", name=entry['name'], link=entry['link'])

            Neo4j.merge(obj, "LawText", "link")

            rel = Neo4Edge(prn,"DEFINE",obj)

            Neo4j.merge(rel)

    def parse_law_conv(self,response):
        for item in response.xpath("//table[@id='page']//table//table//table//table//tr"):
            alias = item.xpath('./td[1]/span/text()').extract()[0].strip()

            entry = AdalaItem(
                fqdn=self.allowed_domains[0],
                link=response.url,
                prnt=response.meta.get('parent',self.start_urls[0]),

                name=alias,
                unit=response.xpath("//div[@class='titre_cadre']/text()").extract()[0].strip(),

                docx=item.xpath('./td[3]/a/@href').extract()[0],
                p_ar=item.xpath('./td[2]/a/@href').extract()[0],
                p_fr=item.xpath('./td[4]/a/@href').extract()[0],
            )

            entry['docx'] = urljoin(response.url, entry['docx']).replace('../','')
            entry['p_ar'] = urljoin(response.url, entry['p_ar'])
            entry['p_fr'] = urljoin(response.url, entry['p_fr'])

            yield entry

            prn = Neo4Node("LawPage", link=entry['prnt'])

            Neo4j.merge(prn, "LawPage", "link")

            obj = Neo4Node("LawConv", name=entry['name'], link=entry['link'])

            Neo4j.merge(obj, "LawConv", "link")

            rel = Neo4Edge(prn,"DEFINE",obj)

            Neo4j.merge(rel)

