# -*- coding: utf-8 -*-
import scrapy

class SggItem(scrapy.Item):
    title = scrapy.Field()
    data = scrapy.Field()

####################################################################################

class SggSpider(scrapy.Spider):
    name = "sgg"
    allowed_domains = ["sgg.gov.ma"]
    start_urls = (
        'http://www.sgg.gov.ma/arabe/CodesTextesLois.aspx',
    )

    def parse(self, response):
        base = "http://www.sgg.gov.ma/arabe"

        yield scrapy.Request(base+"/CodesTextesLois.aspx", callback=self.parse_lois)
        yield scrapy.Request(base+"/Associations.aspx",    callback=self.parse_asso)

        yield scrapy.Request(base+"/Legislations/DernierBulletinOfficiel.aspx",    callback=self.parse_curr)
        yield scrapy.Request(base+"/Legislations/BulletinsOfficielsAns.aspx",    callback=self.parse_last)

    ################################################################################

    def parse_this(self,response):
        #item['title']=response.meta['name']

        for item in response.xpath("//table[@id='page']//table//table//table//table//tr"):
            alias = item.xpath('./td[1]/span/text()').extract().strip()

            entry = AdalaItem(
                fqdn=self.allowed_domains[0],
                link=response.url,

                name=alias,
                unit=response.xpath("//div[@class='titre_cadre']/text()").extract().strip(),

                docx=item.xpath('./td[3]/a/@href').extract(),
                p_ar=item.xpath('./td[2]/a/@href').extract(),
                p_fr=item.xpath('./td[4]/a/@href').extract(),
            )

            entry.docx = urljoin(response.url, entry.docx).replace('../','')
            entry.p_ar = urljoin(response.url, entry.p_ar)
            entry.p_fr = urljoin(response.url, entry.p_fr)

            yield entry

    #*******************************************************************************

    def parse_lois(self,response):
        for item in context.xpath("//div[@class='NormalSite']//p/a"):
            entry = Reactor.entity('texte-loi',
                fqdn=Reactor.domain,
                text=item.xpath('./text()').get(),
                link=item.xpath('./@href').get(),
            )

            for flag in ['pdf']:
                if flag in entry.link:
                    entry.link = urljoin(context.target, entry.link)
                    entry.file = os.path.basename(urlparse(entry.link).path)

                    entry.name,entry.type = os.path.splitext(entry.file)

                    yield entry

    #*******************************************************************************

    def parse_asso(self,response):
        for item in context.xpath("//a[contains(@id,'_lstLinks_linkHyp_')]"):
            entry = Reactor.entity('texte-loi',

                fqdn=Reactor.domain,
                when=str(datetime.now()), # item.xpath(".//tr[3]/td/text()").extract()
                name=item.xpath('./text()').get(),
                link=item.xpath('./@href').get(),
            )

            for flag in ['pdf']:
                if flag in entry.link:
                    entry.link = urljoin(target, entry.link)
                    entry.file = os.path.basename(urlparse(entry.link).path)

                    yield entry
                else:
                    print entry.link

    #*******************************************************************************

    def parse_curr(self,response):
        item = context.xpath("//div[@class='dnnForm']//table")

        entry = Reactor.entity('bulletin',
            fqdn=Reactor.domain,
            when=item.xpath(".//tr[3]/td/text()").get(),
            code=item.xpath('.//tr[4]//p/text()').get(),
            link=item.xpath('.//tr[2]//a/@href').get(),
        )

        for flag in ['pdf']:
            if flag in entry.link:
                entry.link = urljoin(target, entry.link)
                entry.file = os.path.basename(urlparse(entry.link).path)

                yield entry

    #*******************************************************************************

    def parse_last(context, **options):
        for item in context.xpath("//div[@class='dnnForm']//table//tr"):
            entry = Reactor.entity('bulletin',
                fqdn=Reactor.domain,
                when=item.xpath(".//td[2]/text()").get(),
                name=item.xpath('.//td[3]//p/text()').get(),
                link=item.xpath('.//td[1]//a/@href').get(),
            )

            #item['when'] = prnt.xpath(".//td/text()").extract()
            #item['name'] = prnt.xpath(".//p/text()").extract()
            #item['link'] = prnt.xpath(".//a/@href").extract()

            yield entry

