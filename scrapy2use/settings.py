# -*- coding: utf-8 -*-

import os

from decouple import config

BOT_NAME = 'scrapy2use'

SPIDER_MODULES = [
    'scrapy2use.spiders',
    ##'crawl.spider.corporate',
    #'crawl.spider.gov_ma',
    ##'crawl.spider.research',
    'crawl.it_issal.spider',
    'crawl.juridic.spider',
    'crawl.kuchin.spider',
    'crawl.medicinal.spider',
    'crawl.phybase.spider',
]
NEWSPIDER_MODULE = 'scrapy2use.spiders'

JSON_FOLDER = os.path.basename(os.path.basename(__file__))

PARSE_APP_ID = config('APP_ID','neurochip')
PARSE_APP_URL = config('APP_URL','https://localhost:9001/model/api')
PARSE_MASTER = config('APP_MASTER_K','9d114265e6044e94')
PARSE_REST_KEY = config('APP_REST_API','21536c5e-c1dd-4c76-a6b1-51a84c537420')
PARSE_DEFAULT = config('PARSE_DEFAULT','kuchin')
PARSE_WORLDS = config('PARSE_WORLDS','it-issal,kuchin,juridic,medicinal,phybase').split(',')

COOKIES_ENABLED = False
USER_AGENT = 'Scrapy2use (+http://scrapy2use.github.io)'
ROBOTSTXT_OBEY = True

CONCURRENT_REQUESTS = 16
TELNETCONSOLE_ENABLED = False

# Configure a delay for requests for the same website (default: 0)
DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
CONCURRENT_REQUESTS_PER_DOMAIN = 8
CONCURRENT_REQUESTS_PER_IP = 8
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
AUTOTHROTTLE_ENABLED = True
# The initial download delay
AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    #'scrapy2use.pipelines.SQLdb.MySQL': 50,
    #'scrapy2use.pipelines.NoSQL.ParseServer': 50,
    #'scrapy2use.pipelines.Graph.Neo4j': 50,
}
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
SPIDER_MIDDLEWARES = {
#    'scrapy2use.middlewares.HealthSpiderMiddleware': 543,
}
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
#    'scrapy2use.middlewares.HealthDownloaderMiddleware': 543,
}
# See https://docs.scrapy.org/en/latest/topics/extensions.html
EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
}

# Enable and configure the AutoThrottle extension (disabled by default)

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'
