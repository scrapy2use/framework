import os,sys,re,hashlib

from datetime import datetime

import json,yaml

from urllib.parse import *

import scrapy

from scrapy.exceptions import DropItem
from scrapy.http import Request
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import Rule
from scrapy.spiders import Spider as BasicSpider
from scrapy.spiders import CrawlSpider

from scrapy2use import settings

from scrapy2use.items import *
